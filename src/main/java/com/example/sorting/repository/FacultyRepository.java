package com.example.sorting.repository;

import com.example.sorting.entity.Faculty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FacultyRepository extends JpaRepository<Faculty, Long> {
    Optional<Faculty> findByIdAndDeletedFalse(Long id);

    List<Faculty> findAllByDeletedFalse();
    
}
