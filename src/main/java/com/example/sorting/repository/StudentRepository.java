package com.example.sorting.repository;

import com.example.sorting.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> findByIdAndDeletedFalse(Long id);

    List<Student> findAllByDeletedFalse();

    Long countByGroupIdAndDeletedFalse(Long groupId);

    List<Student> findByNameAndDeletedFalse(String name);
}
