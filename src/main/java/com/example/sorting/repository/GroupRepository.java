package com.example.sorting.repository;

import com.example.sorting.entity.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    Optional<Group> findByIdAndDeletedFalse(Long id);

    List<Group> findAllByDeletedFalse();

    List<Group> findAllByFacultyIdAndDeletedFalse(Long facId);
}
