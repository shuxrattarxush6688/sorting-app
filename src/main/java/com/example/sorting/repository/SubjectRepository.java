package com.example.sorting.repository;

import com.example.sorting.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {
    Optional<Subject> findByIdAndDeletedFalse(Long id);

    List<Subject> findAllByDeletedFalse();

    @Query(value = "(select *\n" +
            "from subject sub\n" +
            "where sub.id in (select subjects_id\n" +
            "             from groups_subjects\n" +
            "             where groups_id = (select group_id from student s where s.id = ?1 and s.deleted=false)) and sub.deleted=false )", nativeQuery = true)
    List<Subject> findAllByStudentId(Long studentId);
}
