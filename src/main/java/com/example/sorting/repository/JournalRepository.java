package com.example.sorting.repository;

import com.example.sorting.entity.Journal;
import com.example.sorting.entity.Student;
import com.example.sorting.entity.University;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface JournalRepository extends JpaRepository<Journal, Long> {
    Optional<Journal> findByIdAndDeletedFalse(Long id);

    List<Journal> findAllByDeletedFalse();
}
