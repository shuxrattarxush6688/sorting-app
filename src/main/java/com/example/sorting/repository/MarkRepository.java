package com.example.sorting.repository;

import com.example.sorting.entity.Journal;
import com.example.sorting.entity.Mark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MarkRepository extends JpaRepository<Mark, Long> {
    Optional<Mark> findByIdAndDeletedFalse(Long id);

    List<Mark> findAllByDeletedFalse();
}
