package com.example.sorting.service;

import com.example.sorting.dto.ResultDto;
import com.example.sorting.dto.UniversityDto;

import java.util.List;

public interface UniversityService {

    UniversityDto add(UniversityDto dto);

    UniversityDto update(UniversityDto universityDto, Long id);

    ResultDto delete(Long id);

    UniversityDto getOne(Long id);

    List<UniversityDto> getAll();
}
