package com.example.sorting.service;

import com.example.sorting.dto.GroupDto;
import com.example.sorting.dto.ResultDto;

import java.util.List;

public interface GroupService {

    GroupDto add(GroupDto dto);

    GroupDto update(GroupDto dto, Long id);

    ResultDto delete(Long id);

    GroupDto getOne(Long id);

    List<GroupDto> getAll();

    List<GroupDto> findAllByFacultyId(Long facId);
}
