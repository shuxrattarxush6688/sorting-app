package com.example.sorting.service;

import com.example.sorting.dto.JournalDto;
import com.example.sorting.dto.ResultDto;

import java.util.List;

public interface JournalService {
    JournalDto add(JournalDto dto);

    JournalDto update(JournalDto dto, Long id);

    ResultDto delete(Long id);

    JournalDto getOne(Long id);

    List<JournalDto> getAll();
}
