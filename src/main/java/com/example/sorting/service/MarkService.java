package com.example.sorting.service;

import com.example.sorting.dto.MarkDto;
import com.example.sorting.dto.ResultDto;

import java.util.List;

public interface MarkService {
    MarkDto add(MarkDto dto);

    MarkDto update(MarkDto dto, Long id);

    ResultDto delete(Long id);

    MarkDto getOne(Long id);

    List<MarkDto> getAll();
}
