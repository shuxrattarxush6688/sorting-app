package com.example.sorting.service;

import com.example.sorting.dto.ResultDto;
import com.example.sorting.dto.SubjectDto;

import java.util.List;

public interface SubjectService {
    SubjectDto add(SubjectDto dto);

    SubjectDto update(SubjectDto dto, Long id);

    ResultDto delete(Long id);

    SubjectDto getOne(Long id);

    List<SubjectDto> getAll();

    List<SubjectDto> findAllByStudentId(Long studentId);
}
