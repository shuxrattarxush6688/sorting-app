package com.example.sorting.service;

import com.example.sorting.dto.FacultyDto;
import com.example.sorting.dto.ResultDto;

import java.util.List;

public interface FacultyService {

    FacultyDto add(FacultyDto dto);

    FacultyDto update(FacultyDto facultyDto, Long id);

    ResultDto delete(Long id);

    FacultyDto getOne(Long id);

    List<FacultyDto> getAll();
}
