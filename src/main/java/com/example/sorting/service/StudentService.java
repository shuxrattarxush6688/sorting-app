package com.example.sorting.service;

import com.example.sorting.dto.ResultDto;
import com.example.sorting.dto.StudentAboutDto;
import com.example.sorting.dto.StudentDto;

import java.util.List;

public interface StudentService {
    StudentDto add(StudentDto dto);

    StudentDto update(StudentDto dto, Long id);

    ResultDto delete(Long id);

    StudentDto getOne(Long id);

    List<StudentDto> getAll();

    List<StudentAboutDto> findByNameAllAbout(String name);
}
