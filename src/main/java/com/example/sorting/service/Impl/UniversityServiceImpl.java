package com.example.sorting.service.Impl;

import com.example.sorting.dto.ResultDto;
import com.example.sorting.dto.UniversityDto;
import com.example.sorting.entity.University;
import com.example.sorting.exception.SortingException;
import com.example.sorting.repository.UniversityRepository;
import com.example.sorting.service.UniversityService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UniversityServiceImpl implements UniversityService {

    private final UniversityRepository universityRepository;

    @Override
    public UniversityDto add(UniversityDto dto) {
        University newUniversity = new University();
        newUniversity.setAddress(dto.getAddress());
        newUniversity.setName(dto.getName());
        newUniversity.setOpenYear(dto.getOpenYear());
        return UniversityDto.toDto(universityRepository.save(newUniversity));
    }

    @Override
    public UniversityDto update(UniversityDto universityDto, Long id) {
        University university = universityRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("University not found by id=" + id));
        university.setName(universityDto.getName());
        university.setAddress(universityDto.getAddress());
        university.setOpenYear(universityDto.getOpenYear());
        return UniversityDto.toDto(universityRepository.save(university));
    }

    @Override
    public ResultDto delete(Long id) {
        University university = universityRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("University not found by id=" + id));
        university.setDeleted(true);
        universityRepository.save(university);
        return ResultDto.of("Success", true);
    }

    @Override
    public UniversityDto getOne(Long id) {
        return UniversityDto.toDto(universityRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("University not found by id=" + id)));
    }

    @Override
    public List<UniversityDto> getAll() {
        return universityRepository.findAllByDeletedFalse().stream().map(UniversityDto::toDto).collect(Collectors.toList());
    }
}
