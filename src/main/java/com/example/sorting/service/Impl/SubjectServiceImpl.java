package com.example.sorting.service.Impl;

import com.example.sorting.dto.ResultDto;
import com.example.sorting.dto.SubjectDto;
import com.example.sorting.entity.Subject;
import com.example.sorting.exception.SortingException;
import com.example.sorting.repository.GroupRepository;
import com.example.sorting.repository.SubjectRepository;
import com.example.sorting.service.SubjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SubjectServiceImpl implements SubjectService {

    private final SubjectRepository subjectRepository;
    private final GroupRepository groupRepository;

    @Override
    public SubjectDto add(SubjectDto dto) {

        Subject newSubject = new Subject();
        newSubject.setName(dto.getName());
        return SubjectDto.toDto(subjectRepository.save(newSubject));
    }

    @Override
    public SubjectDto update(SubjectDto dto, Long id) {
        Subject subject = subjectRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("Subject not found by id=" + id));

        subject.setName(dto.getName());
        return SubjectDto.toDto(subjectRepository.save(subject));
    }

    @Override
    public ResultDto delete(Long id) {
        Subject subject = subjectRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("Subject not found by id=" + id));
        subject.setDeleted(true);
        subjectRepository.save(subject);
        return ResultDto.of("Success", true);
    }

    @Override
    public SubjectDto getOne(Long id) {
        return SubjectDto.toDto(subjectRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("Subject not found by id=" + id)));
    }

    @Override
    public List<SubjectDto> getAll() {
        return subjectRepository.findAllByDeletedFalse().stream().map(SubjectDto::toDto).collect(Collectors.toList());
    }

    @Override
    public List<SubjectDto> findAllByStudentId(Long studentId) {
        return subjectRepository.findAllByStudentId(studentId).stream().map(SubjectDto::toDto).collect(Collectors.toList());
    }
}
