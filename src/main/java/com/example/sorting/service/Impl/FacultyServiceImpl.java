package com.example.sorting.service.Impl;

import com.example.sorting.dto.FacultyDto;
import com.example.sorting.dto.ResultDto;
import com.example.sorting.entity.Faculty;
import com.example.sorting.entity.University;
import com.example.sorting.exception.SortingException;
import com.example.sorting.repository.FacultyRepository;
import com.example.sorting.repository.GroupRepository;
import com.example.sorting.repository.UniversityRepository;
import com.example.sorting.service.FacultyService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FacultyServiceImpl implements FacultyService {

    private final FacultyRepository facultyRepository;
    private final UniversityRepository universityRepository;
    private final GroupRepository groupRepository;

    @Override
    public FacultyDto add(FacultyDto dto) {
        Optional<University> university = universityRepository.findByIdAndDeletedFalse(dto.getUniversityId());
        if (university.isEmpty()) {
            throw new SortingException("University not found by id=" + dto.getUniversityId());
        }
        Faculty newFaculty = new Faculty();
        newFaculty.setName(dto.getName());
        newFaculty.setUniversity(university.get());
        return FacultyDto.toDto(facultyRepository.save(newFaculty));
    }

    @Override
    public FacultyDto update(FacultyDto dto, Long id) {
        Faculty faculty = facultyRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("Faculty not found by id=" + id));
        Optional<University> university = universityRepository.findByIdAndDeletedFalse(dto.getUniversityId());
        if (university.isEmpty()) {
            throw new SortingException("University not found by id=" + dto.getUniversityId());
        }
        faculty.setName(dto.getName());
        faculty.setUniversity(university.get());
        return FacultyDto.toDto(facultyRepository.save(faculty));
    }

    @Override
    public ResultDto delete(Long id) {
        Faculty faculty = facultyRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("Faculty not found by id=" + id));
        faculty.setDeleted(true);
        facultyRepository.save(faculty);
        return ResultDto.of("Success", true);
    }

    @Override
    public FacultyDto getOne(Long id) {
        return FacultyDto.toDto(facultyRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("Faculty not found by id=" + id)));
    }

    @Override
    public List<FacultyDto> getAll() {
        return facultyRepository.findAllByDeletedFalse().stream().map(FacultyDto::toDto).collect(Collectors.toList());
    }
}
