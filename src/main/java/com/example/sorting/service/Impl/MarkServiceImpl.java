package com.example.sorting.service.Impl;

import com.example.sorting.dto.MarkDto;
import com.example.sorting.dto.ResultDto;
import com.example.sorting.entity.Journal;
import com.example.sorting.entity.Mark;
import com.example.sorting.entity.Student;
import com.example.sorting.exception.SortingException;
import com.example.sorting.repository.JournalRepository;
import com.example.sorting.repository.MarkRepository;
import com.example.sorting.repository.StudentRepository;
import com.example.sorting.service.MarkService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MarkServiceImpl implements MarkService {

    private final MarkRepository markRepository;
    private final StudentRepository studentRepository;
    private final JournalRepository journalRepository;

    @Override
    public MarkDto add(MarkDto dto) {
        Student student = studentRepository.findByIdAndDeletedFalse(dto.getStudentId()).orElseThrow(() ->
                new SortingException("Student not found by id=" + dto.getStudentId()));
        Journal journal = journalRepository.findByIdAndDeletedFalse(dto.getJournalId()).orElseThrow(() ->
                new SortingException("Student not found by id=" + dto.getJournalId()));
        Mark mark = new Mark();
        mark.setValue(dto.getValue());
        mark.setStudent(student);
        mark.setJournal(journal);
        return MarkDto.toDto(markRepository.save(mark));
    }

    @Override
    public MarkDto update(MarkDto dto, Long id) {
        Mark mark = markRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new SortingException("Student not found by id=" + id));
        Student student = studentRepository.findByIdAndDeletedFalse(dto.getStudentId()).orElseThrow(() ->
                new SortingException("Student not found by id=" + dto.getStudentId()));
        Journal journal = journalRepository.findByIdAndDeletedFalse(dto.getJournalId()).orElseThrow(() ->
                new SortingException("Student not found by id=" + dto.getJournalId()));
        mark.setValue(dto.getValue());
        mark.setStudent(student);
        mark.setJournal(journal);
        return MarkDto.toDto(markRepository.save(mark));
    }

    @Override
    public ResultDto delete(Long id) {
        Mark mark = markRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new SortingException("Student not found by id=" + id));
        mark.setDeleted(true);
        markRepository.save(mark);
        return ResultDto.of("Success", true);
    }

    @Override
    public MarkDto getOne(Long id) {
        return MarkDto.toDto(markRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new SortingException("Student not found by id=" + id)));
    }

    @Override
    public List<MarkDto> getAll() {
        return markRepository.findAllByDeletedFalse().stream().map(MarkDto::toDto).collect(Collectors.toList());
    }
}
