package com.example.sorting.service.Impl;

import com.example.sorting.dto.JournalDto;
import com.example.sorting.dto.ResultDto;
import com.example.sorting.entity.Group;
import com.example.sorting.entity.Journal;
import com.example.sorting.entity.Subject;
import com.example.sorting.exception.SortingException;
import com.example.sorting.repository.GroupRepository;
import com.example.sorting.repository.JournalRepository;
import com.example.sorting.repository.SubjectRepository;
import com.example.sorting.service.JournalService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JournalServiceImpl implements JournalService {

    private final JournalRepository journalRepository;
    private final GroupRepository groupRepository;
    private final SubjectRepository subjectRepository;

    @Override
    public JournalDto add(JournalDto dto) {
        Group group = groupRepository.findByIdAndDeletedFalse(dto.getGroupId()).orElseThrow(() ->
                new SortingException("Group not found by id=" + dto.getGroupId()));
        Journal newJournal = new Journal();
        newJournal.setName(dto.getName());
        newJournal.setGroup(group);
        if (dto.getSubjectsIds().isEmpty() || dto.getSubjectsIds() == null) {
            newJournal.setSubjects(null);
        } else {
            List<Subject> subjects = new ArrayList<>();
            for (Long id : dto.getSubjectsIds()) {
                Subject subject = subjectRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                        new SortingException("Subject not found by id=" + id));
                subjects.add(subject);
            }
            newJournal.setSubjects(subjects);
        }
        return JournalDto.toDto(journalRepository.save(newJournal));

    }

    @Override
    public JournalDto update(JournalDto dto, Long id) {
        Journal journal = journalRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new SortingException("Journal not found by id=" + id));
        Group group = groupRepository.findByIdAndDeletedFalse(dto.getGroupId()).orElseThrow(() ->
                new SortingException("Group not found by id=" + dto.getGroupId()));
        journal.setName(dto.getName());
        journal.setGroup(group);
        if (!dto.getSubjectsIds().isEmpty() || dto.getSubjectsIds() != null) {
            List<Subject> newSubjects = new ArrayList<>();
            for (Long subId : dto.getSubjectsIds()) {
                Subject subject = subjectRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                        new SortingException("Subject not found by id=" + subId));
                newSubjects.add(subject);
                journal.setSubjects(newSubjects);
            }
        }
        return JournalDto.toDto(journalRepository.save(journal));
    }

    @Override
    public ResultDto delete(Long id) {
        Journal journal = journalRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new SortingException("Journal not found by id=" + id));
        journal.setDeleted(true);
        journalRepository.save(journal);
        return ResultDto.of("Success", true);
    }

    @Override
    public JournalDto getOne(Long id) {
        return JournalDto.toDto(journalRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new SortingException("Journal not found by id=" + id)));
    }

    @Override
    public List<JournalDto> getAll() {
        return journalRepository.findAllByDeletedFalse().stream().map(JournalDto::toDto).collect(Collectors.toList());
    }
}
