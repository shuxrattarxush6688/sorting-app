package com.example.sorting.service.Impl;

import com.example.sorting.dto.ResultDto;
import com.example.sorting.dto.StudentAboutDto;
import com.example.sorting.dto.StudentDto;
import com.example.sorting.dto.SubjectDto;
import com.example.sorting.entity.Group;
import com.example.sorting.entity.Student;
import com.example.sorting.exception.SortingException;
import com.example.sorting.repository.FacultyRepository;
import com.example.sorting.repository.GroupRepository;
import com.example.sorting.repository.StudentRepository;
import com.example.sorting.repository.SubjectRepository;
import com.example.sorting.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;
    private final FacultyRepository facultyRepository;

    @Override
    public StudentDto add(StudentDto dto) {
        Group group = groupRepository.findByIdAndDeletedFalse(dto.getGroupId()).orElseThrow(() ->
                new SortingException("Group not found by id=" + dto.getGroupId()));
        ;
        Student newStudent = new Student();
        newStudent.setName(dto.getName());
        newStudent.setLastname(dto.getLastname());
        newStudent.setGroup(group);
        newStudent.setCourse(dto.getCourse());
        return StudentDto.toDto(studentRepository.save(newStudent));
    }

    @Override
    public StudentDto update(StudentDto dto, Long id) {
        Student student = studentRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new SortingException("Student not found by id=" + id));
        Group group = groupRepository.findByIdAndDeletedFalse(dto.getGroupId()).orElseThrow(() ->
                new SortingException("Group not found by id=" + dto.getGroupId()));
        ;
        student.setName(dto.getName());
        student.setLastname(dto.getLastname());
        student.setGroup(group);
        student.setCourse(dto.getCourse());
        return StudentDto.toDto(studentRepository.save(student));
    }

    @Override
    public ResultDto delete(Long id) {
        Student student = studentRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new SortingException("Student not found by id=" + id));
        student.setDeleted(true);
        studentRepository.save(student);
        return ResultDto.of("Success", true);
    }

    @Override
    public StudentDto getOne(Long id) {
        return StudentDto.toDto(studentRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                new SortingException("Student not found by id=" + id)));
    }

    @Override
    public List<StudentDto> getAll() {
        return studentRepository.findAllByDeletedFalse().stream().map(StudentDto::toDto).collect(Collectors.toList());
    }

    @Override
    public List<StudentAboutDto> findByNameAllAbout(String name) {
        return  studentRepository.findByNameAndDeletedFalse(name).stream().map(student ->
                StudentAboutDto.toDto(student.getGroup(),student.getGroup().getFaculty())).collect(Collectors.toList());
    }

}
