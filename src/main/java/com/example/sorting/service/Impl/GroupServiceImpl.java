package com.example.sorting.service.Impl;

import com.example.sorting.dto.GroupDto;
import com.example.sorting.dto.ResultDto;
import com.example.sorting.entity.Faculty;
import com.example.sorting.entity.Group;
import com.example.sorting.entity.Subject;
import com.example.sorting.exception.SortingException;
import com.example.sorting.repository.FacultyRepository;
import com.example.sorting.repository.GroupRepository;
import com.example.sorting.repository.StudentRepository;
import com.example.sorting.repository.SubjectRepository;
import com.example.sorting.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;
    private final FacultyRepository facultyRepository;
    private final SubjectRepository subjectRepository;
    private final StudentRepository studentRepository;

    @Override
    public GroupDto add(GroupDto dto) {
        Optional<Faculty> faculty = facultyRepository.findByIdAndDeletedFalse(dto.getFacultyId());
        if (faculty.isEmpty()) {
            throw new SortingException("Faculty not found by id=" + dto.getFacultyId());
        }
        Group newGroup = new Group();
        newGroup.setName(dto.getName());
        newGroup.setFaculty(faculty.get());

        if (dto.getSubjectIds().isEmpty() || dto.getSubjectIds() == null) {
            newGroup.setSubjects(null);
        } else {
            List<Subject> subjects = new ArrayList<>();
            for (Long id : dto.getSubjectIds()) {
                Subject subject = subjectRepository.findByIdAndDeletedFalse(id).orElseThrow(() ->
                        new SortingException("Subject not found by id=" + id));
                subjects.add(subject);
            }
            newGroup.setSubjects(subjects);
        }
        return GroupDto.toDto(groupRepository.save(newGroup));
    }

    @Override
    public GroupDto update(GroupDto dto, Long id) {
        Group group = groupRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("Group not found by id=" + id));
        Optional<Faculty> faculty = facultyRepository.findByIdAndDeletedFalse(dto.getFacultyId());
        if (faculty.isEmpty()) {
            throw new SortingException("Faculty not found by id=" + dto.getFacultyId());
        }
        group.setName(dto.getName());
        group.setFaculty(faculty.get());
        if (!dto.getSubjectIds().isEmpty() || dto.getSubjectIds() != null) {
            List<Subject> subjects = new ArrayList<>();
            for (Long subId : dto.getSubjectIds()) {
                Subject subject = subjectRepository.findByIdAndDeletedFalse(subId).orElseThrow(() ->
                        new SortingException("Subject not found by id=" + subId));
                subjects.add(subject);
            }
            group.setSubjects(subjects);
        }
        return GroupDto.toDto(groupRepository.save(group));
    }

    @Override
    public ResultDto delete(Long id) {
        Group group = groupRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("Group not found by id=" + id));
        group.setDeleted(true);
        groupRepository.save(group);
        return ResultDto.of("Success", true);
    }

    @Override
    public GroupDto getOne(Long id) {
        return GroupDto.toDto(groupRepository.findByIdAndDeletedFalse(id).orElseThrow(() -> new SortingException("Group not found by id=" + id)));
    }

    @Override
    public List<GroupDto> getAll() {
        return groupRepository.findAllByDeletedFalse().stream().map(GroupDto::toDto).collect(Collectors.toList());
    }

    @Override
    public List<GroupDto> findAllByFacultyId(Long facId) {
        return groupRepository.findAllByFacultyIdAndDeletedFalse(facId).stream().map(group -> {
            group.setCount(studentRepository.countByGroupIdAndDeletedFalse(group.getId()));
            return GroupDto.toDto(group);
        }).collect(Collectors.toList());
    }
}
