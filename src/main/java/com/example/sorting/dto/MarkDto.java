package com.example.sorting.dto;

import com.example.sorting.entity.Mark;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
public class MarkDto {
    private Long id;
    private Integer value;
    private Long studentId;
        private Long journalId;

    public static MarkDto toDto(Mark mark) {
        return MarkDto.of(mark.getId(), mark.getValue(), mark.getStudent().getId(), mark.getJournal().getId());
    }
}
