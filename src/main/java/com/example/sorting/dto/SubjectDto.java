package com.example.sorting.dto;

import com.example.sorting.entity.Subject;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubjectDto {
    private Long id;
    private String name;

    public static SubjectDto toDto(Subject subject) {
        return SubjectDto.of(subject.getId(), subject.getName());
    }
}
