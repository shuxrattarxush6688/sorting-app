package com.example.sorting.dto;

import com.example.sorting.entity.Faculty;
import com.example.sorting.entity.Group;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
public class StudentAboutDto {
    private GroupDto group;
    private FacultyDto faculty;

    public static StudentAboutDto toDto(Group group, Faculty faculty){
        return  StudentAboutDto.of(GroupDto.toDto(group),FacultyDto.toDto(faculty));
    }
}
