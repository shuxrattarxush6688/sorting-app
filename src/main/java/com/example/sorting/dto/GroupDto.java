package com.example.sorting.dto;

import com.example.sorting.entity.BaseEntity;
import com.example.sorting.entity.Group;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GroupDto {

    private Long id;

    private String name;

    private Long facultyId;

    private List<Long> subjectIds;

    private Long studentsCount;

    public static GroupDto toDto(Group group) {
        return GroupDto.of(group.getId(), group.getName(), group.getFaculty().getId(),group.getSubjects().stream()
                .map(BaseEntity::getId).toList(), group.getCount());
    }

}
