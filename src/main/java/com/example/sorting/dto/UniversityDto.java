package com.example.sorting.dto;

import com.example.sorting.entity.University;
import lombok.Data;

@Data
public class UniversityDto {
    private Long id;
    private String name;
    private String address;
    private int openYear;

    public static UniversityDto toDto(University university){
        UniversityDto universityDto = new UniversityDto();
        universityDto.setId(university.getId());
        universityDto.setName(university.getName());
        universityDto.setAddress(university.getAddress());
        universityDto.setOpenYear(university.getOpenYear());
        return universityDto;
    }
}
