package com.example.sorting.dto;

import com.example.sorting.entity.Faculty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
public class FacultyDto {
    private Long id;
    private String name;
    private Long universityId;

    public static FacultyDto toDto(Faculty faculty) {
        return FacultyDto.of(faculty.getId(), faculty.getName(), faculty.getUniversity().getId());
    }
}
