package com.example.sorting.dto;

import com.example.sorting.entity.BaseEntity;
import com.example.sorting.entity.Journal;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor(staticName = "of")
public class JournalDto {
    private Long id;
    private String name;
    private Long groupId;
    private List<Long> subjectsIds;

    public static JournalDto toDto(Journal journal) {
        return JournalDto.of(journal.getId(), journal.getName(), journal.getGroup().getId(), journal.getSubjects().stream()
                .map(BaseEntity::getId).toList());
    }
}
