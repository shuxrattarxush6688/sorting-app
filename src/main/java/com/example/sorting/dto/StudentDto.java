package com.example.sorting.dto;

import com.example.sorting.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
public class StudentDto {
    private Long id;
    private String name;
    private String lastname;
    private Integer course;
    private Long groupId;

    public static StudentDto toDto(Student student) {
        return StudentDto.of(student.getId(), student.getName(), student.getLastname(), student.getCourse(), student.getGroup().getId());
    }
}
