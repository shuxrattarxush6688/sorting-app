package com.example.sorting.exception;

public class SortingException extends RuntimeException {

    public SortingException(String message) {
        super(message);
    }
}
