package com.example.sorting.controller;

import com.example.sorting.dto.GroupDto;
import com.example.sorting.dto.ResultDto;
import com.example.sorting.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/group")
@RequiredArgsConstructor
public class GroupController {
    private final GroupService groupService;

    @PostMapping
    public GroupDto add(@RequestBody GroupDto groupDto){
        return groupService.add(groupDto);
    }

    @PutMapping("/{id}")
    public GroupDto update(@RequestBody GroupDto dto, @PathVariable Long id){
        return groupService.update(dto, id);
    }

    @DeleteMapping("/{id}")
    public ResultDto delete(@PathVariable Long id){
        return groupService.delete(id);
    }

    @GetMapping("/{id}")
    public GroupDto getOne(@PathVariable Long id){
        return groupService.getOne(id);
    }

    @GetMapping
    public List<GroupDto> getAll(){
        return groupService.getAll();
    }

    @GetMapping("/faculty/{id}")
    public List<GroupDto> getAllByFacId(@PathVariable Long id){
        return groupService.findAllByFacultyId(id);
    }

}
