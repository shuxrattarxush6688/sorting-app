package com.example.sorting.controller;

import com.example.sorting.dto.ResultDto;
import com.example.sorting.dto.StudentAboutDto;
import com.example.sorting.dto.StudentDto;
import com.example.sorting.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/student")
public class StudentController {
    private final StudentService studentService;

    @PostMapping
    public StudentDto add(@RequestBody StudentDto dto) {
        return studentService.add(dto);
    }

    @PutMapping("/{id}")
    public StudentDto update(@RequestBody StudentDto dto, @PathVariable Long id) {
        return studentService.update(dto, id);
    }

    @DeleteMapping("/{id}")
    public ResultDto delete(@PathVariable Long id) {
        return studentService.delete(id);
    }

    @GetMapping("/{id}")
    public StudentDto getOne(@PathVariable Long id) {
        return studentService.getOne(id);
    }

    @GetMapping
    public List<StudentDto> getAll() {
        return studentService.getAll();
    }


    @GetMapping("/search")
    public List<StudentAboutDto> getByStudentName(@RequestParam String name) {
        return studentService.findByNameAllAbout(name);
    }

}
