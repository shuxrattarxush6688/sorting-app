package com.example.sorting.controller;

import com.example.sorting.dto.ResultDto;
import com.example.sorting.dto.SubjectDto;
import com.example.sorting.service.SubjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/subject")
@RequiredArgsConstructor
public class SubjectController {
    private final SubjectService subjectService;

    @PostMapping
    public SubjectDto add(@RequestBody SubjectDto dto){
        return subjectService.add(dto);
    }

    @PutMapping("/{id}")
    public SubjectDto update(@RequestBody SubjectDto dto, @PathVariable Long id){
        return subjectService.update(dto, id);
    }

    @DeleteMapping("{id}")
    public ResultDto delete(@PathVariable Long id) {
       return subjectService.delete(id);
    }

    @GetMapping("/{id}")
    public SubjectDto getOne(@PathVariable Long id){
        return subjectService.getOne(id);
    }

    @GetMapping
    public List<SubjectDto> getAll(){
        return subjectService.getAll();
    }

    @GetMapping("/student/{id}")
    public List<SubjectDto> findAllByStudentId(@PathVariable Long id){
        return subjectService.findAllByStudentId(id);
    }
}
