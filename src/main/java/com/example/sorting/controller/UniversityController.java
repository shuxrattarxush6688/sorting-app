package com.example.sorting.controller;

import com.example.sorting.dto.ResultDto;
import com.example.sorting.dto.UniversityDto;
import com.example.sorting.service.UniversityService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/university")
@RequiredArgsConstructor
public class UniversityController {

    private final UniversityService universityService;

    @PostMapping
    public UniversityDto add(@RequestBody UniversityDto dto) {
        return universityService.add(dto);
    }

    @PutMapping("/{id}")
    public UniversityDto update(@RequestBody UniversityDto dto, @PathVariable Long id) {
        return universityService.update(dto, id);
    }

    @DeleteMapping("/{id}")
    public ResultDto delete(@PathVariable Long id){
        return universityService.delete(id);
    }

    @GetMapping("/{id}")
    public UniversityDto getOne(@PathVariable Long id){
        return universityService.getOne(id);
    }

    @GetMapping
    public List<UniversityDto>getAll(){
        return universityService.getAll();
    }

}
