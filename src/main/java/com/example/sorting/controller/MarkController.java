package com.example.sorting.controller;

import com.example.sorting.dto.MarkDto;
import com.example.sorting.dto.ResultDto;
import com.example.sorting.service.MarkService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/mark")
@RequiredArgsConstructor
public class MarkController {
    private final MarkService markService;

    @PostMapping
    public MarkDto add(@RequestBody MarkDto dto) {
        return markService.add(dto);
    }

    @PutMapping("/{id}")
    public MarkDto update(@RequestBody MarkDto dto, @PathVariable Long id) {
        return markService.update(dto, id);
    }

    @DeleteMapping("/{id}")
    public ResultDto delete(@PathVariable Long id) {
        return markService.delete(id);
    }

    @GetMapping("/{id}")
    public MarkDto getOne(@PathVariable Long id) {
        return markService.getOne(id);
    }

    @GetMapping
    public List<MarkDto> getAll() {
        return markService.getAll();
    }

}
