package com.example.sorting.controller;

import com.example.sorting.dto.JournalDto;
import com.example.sorting.dto.ResultDto;
import com.example.sorting.service.JournalService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/journal")
@RequiredArgsConstructor
public class JournalController {

    private final JournalService journalService;

    @PostMapping
    public JournalDto add(@RequestBody JournalDto dto){
        return journalService.add(dto);
    }

    @PutMapping("/{id}")
    public JournalDto update(@RequestBody JournalDto dto, @PathVariable Long id){
        return journalService.update(dto, id);
    }

    @DeleteMapping("/{id}")
    public ResultDto delete(@PathVariable Long id){
        return journalService.delete(id);
    }

    @GetMapping("/{id}")
    public JournalDto getOne(@PathVariable Long id){
        return journalService.getOne(id);
    }

    @GetMapping
    public List<JournalDto> getAll(){
        return journalService.getAll();
    }

}
