package com.example.sorting.controller;

import com.example.sorting.dto.FacultyDto;
import com.example.sorting.dto.ResultDto;
import com.example.sorting.service.FacultyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/faculty")
@RequiredArgsConstructor
public class FacultyController {

    private final FacultyService facultyService;

    @PostMapping
    public FacultyDto add(@RequestBody FacultyDto dto) {
        return facultyService.add(dto);
    }

    @PutMapping("/{id}")
    public FacultyDto update(@RequestBody FacultyDto dto, @PathVariable Long id) {
        return facultyService.update(dto, id);
    }

    @DeleteMapping("/{id}")
    public ResultDto delete(@PathVariable Long id) {
        return facultyService.delete(id);
    }

    @GetMapping("/{id}")
    public FacultyDto getOne(@PathVariable Long id) {
        return facultyService.getOne(id);
    }

    @GetMapping
    public List<FacultyDto> getAll() {
        return facultyService.getAll();
    }

}
