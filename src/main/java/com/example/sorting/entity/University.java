package com.example.sorting.entity;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class University extends BaseEntity {
    private String name;
    private String address;
    private Integer openYear;
}
