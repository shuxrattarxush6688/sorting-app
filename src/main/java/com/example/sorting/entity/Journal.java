package com.example.sorting.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Data
@Entity
public class Journal extends BaseEntity {
    private String name;
    @OneToOne
    private Group group;
    @ManyToMany
    private List<Subject> subjects;

}
