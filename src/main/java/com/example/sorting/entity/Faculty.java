package com.example.sorting.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@Entity
public class Faculty extends BaseEntity {
    private String name;
    @ManyToOne
    private University university;


}
