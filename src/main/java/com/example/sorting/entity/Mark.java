package com.example.sorting.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Data
public class Mark extends BaseEntity {
    private Integer value;
    @ManyToOne
    private Student student;
    @ManyToOne
    private Journal journal;
}
