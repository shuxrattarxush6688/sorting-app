package com.example.sorting.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Data
public class Student extends BaseEntity {

    private String name;

    private String lastname;

    private Integer course;

    @ManyToOne
    private Group group;

}
