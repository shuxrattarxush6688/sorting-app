package com.example.sorting.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Data
public class Subject extends BaseEntity {
    private String name;
}
