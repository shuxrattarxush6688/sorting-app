package com.example.sorting.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

@Entity(name = "groups")
@Data
public class Group extends BaseEntity {

    private String name;

    @ManyToOne
    private Faculty faculty;

    @ManyToMany
    private List<Subject> subjects;

    transient private Long count;

}
